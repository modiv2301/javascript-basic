import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  count = 0;

  handleIncre= ()=>{
    this.count = this.count+1;
  }
  handleDec= ()=>{
    this.count = this.count-1
  }
  handleReset= ()=>{
    this.count = 0
  }
}
