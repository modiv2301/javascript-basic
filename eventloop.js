(async () => {
    async function call(b) {
        console.log("1");
        b();
        console.log("3");
      console.log("4");
      console.log("5");
    }
    let b = () => {
        console.log("2");
    }
    await call(b);
    // Now the next whole code will be inserted into WebAPI
    console.log("14") 
    console.log("15")
    console.log("16")
    })()
    
    
    console.log("6");
    console.log("7");
    
    (async () => {
    async function call(b) {
        console.log("8");
        b();
        console.log("10");
      console.log("11");
      console.log("12");
    }
    let b = () => {
        console.log("9");
    }
    await call(b);
    // Now the next whole code will be inserted into WebAPI
        console.log("17") // goes in WEB API
    })()
    
    console.log("13")