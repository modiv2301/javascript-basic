import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import { AppComponent } from './app.component'
import { MyButtonComponent } from './button.component'
import { MyHeaderComponent } from './Header/header.componet'

@NgModule({
  imports:[BrowserModule],
  declarations:[AppComponent, MyButtonComponent, MyHeaderComponent],
  bootstrap:[AppComponent]
})
export class AppModule{}
